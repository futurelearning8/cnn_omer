import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.datasets import cifar10
from sklearn.metrics import confusion_matrix
import seaborn as sn
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score
from time import time
from tensorflow.keras import callbacks

class VGG16():
    def __init__(self):
        # (features, kernel_size, stride)

        self.input = tf.keras.Input(shape=(32, 32, 3),  name='input')

        conv1 = ( 64, 3,  (1,1))
        conv2 = (64, 3, (1,1))
        max_pool = ((2,2), (2,2) )
        self.block1 = typeA_block(conv1,conv2,max_pool,self.input).maxpool

        layer = tf.keras.layers.Dropout(.2)
        self.block1_drop_out = layer (self.block1, training=True)
        conv1 = (128, 3, (1, 1))
        conv2 = (128, 3, (1, 1))
        max_pool = ((2, 2), (2, 2))
        self.block2 = typeA_block(conv1,conv2,max_pool,self.block1_drop_out).maxpool

        conv1 = (256, 3, (1, 1))
        conv2 = (256, 3, (1, 1))
        conv3 = (256, 3, (1, 1))
        max_pool = ((2, 2), (2, 2))

        layer = tf.keras.layers.Dropout(.2)
        self.block2_drop_out = layer(self.block2, training=True)
        self.block3 = typeB_block(conv1,conv2,conv3,max_pool,self.block2_drop_out)
        self.avgpool = layers.GlobalAvgPool2D()(self.block3.maxpool)

        layer = tf.keras.layers.Dropout(.2)
        self.avgpool_drop_out = layer(self.avgpool, training=True)

        self.output = layers.Dense(10,activation='softmax')(self.avgpool_drop_out)

        #self.output = layers.Conv2D(10, 1, strides=(1,1),padding='same')(self.avgpool)

        """
        # flatten :
        self.block4 = layers.Flatten()( self.block3.maxpool)
        self.fc1 = layers.Dense(2048)(self.block4)

        # layer = tf.keras.layers.Dropout(.5)#, input_shape=(2,))
        # self.fc1 = layer(self.fc1, training=True)


        self.fc2 = layers.Dense(512)(self.fc1)
        self.output = layers.Dense(10,activation='softmax')(self.fc2)
        # block4 = typeB_block()
        # block5 = typeB_block()
        """
        self.model = tf.keras.Model(inputs=[self.input], outputs=[self.output])



# block 1-2: conv conv maxpool
class typeA_block():
    def __init__(self,conv1,conv2,max_pool,input):
        activation = tf.keras.layers.Activation('relu')

        (features, kernel_size, stride) = conv1
        # .Conv2D(filters, kernel_size, strides=(1, 1), padding='valid')
        self.conv1 = tf.keras.layers.Conv2D( 64, 3, strides=(1,1),padding='same')(input)

        #self.bn1 = tf.keras.layers.BatchNormalization()(self.conv1)

        self.conv1_act = activation(self.conv1)
        (features_num, kernel_size, stride) = conv2
        self.conv2 = tf.keras.layers.Conv2D(features, kernel_size, strides=stride,padding='same')(self.conv1_act)
        (pool_size,stride )=max_pool

        #self.bn2 = tf.keras.layers.BatchNormalization()(self.conv2)
        activation = tf.keras.layers.Activation('relu')

        self.conv2_act = activation(self.conv2)
        self.maxpool = tf.keras.layers.MaxPool2D(pool_size=pool_size, strides= stride,padding='same')(self.conv2_act)

# block 3-5: conv conv conv maxpool
class typeB_block():
    def __init__(self,conv1,conv2,conv3,max_pool,input):

        activation = layers.Activation('relu')

        (features, kernel_size, stride) = conv1
        self.conv1 = layers.Conv2D(features, kernel_size, strides=stride,padding='same')(input)
        #self.bn1 = tf.keras.layers.BatchNormalization()(self.conv1)
        self.conv1_act = activation(self.conv1)

        (features, kernel_size, stride) = conv2
        self.conv2 = layers.Conv2D(features, kernel_size, strides=stride,padding='same')( self.conv1_act)
        #self.bn2 = tf.keras.layers.BatchNormalization()(self.conv2)

        activation = layers.Activation('relu')
        self.conv2_act = activation(self.conv2)

        (features, kernel_size, stride) = conv3
        self.conv3 =layers.Conv2D(features, kernel_size, strides=stride,padding='same')( self.conv2_act)
        #self.bn3 = tf.keras.layers.BatchNormalization()(self.conv3)

        layer = tf.keras.layers.Dropout(.3)
        self.conv3_drop_out = layer(self.conv3, training=True)


        activation = layers.Activation('relu')
        self.conv3_act = activation(self.conv3_drop_out)


        (pool_size, stride) = max_pool
        self.maxpool = tf.keras.layers.MaxPool2D(pool_size=pool_size, strides=stride,padding='same')( self.conv3_act )


def normalize(x):
    # convert from integers to floats
    x = x.astype('float32')
    # normalize to the range 0-1
    x /= 255.0
    return x
def main():
    (x_train, y_train), (x_test, y_test) = cifar10.load_data()
    x_train = normalize(x_train)
    x_test = normalize(x_test)
    vgg_model = VGG16()
    #output = vgg_model.fit()
    vgg_model.model.compile(optimizer='Adam',loss='SparseCategoricalCrossentropy',metrics=['accuracy'])
    checkpoint = tf.keras.callbacks.ModelCheckpoint(filepath='model.{epoch:02d}-{val_loss:.2f}.h5')
    tensorboard =  tf.keras.callbacks.TensorBoard(log_dir='./logs{}'.format(time()))
    lru_plato = tf.keras.callbacks.ReduceLROnPlateau()
    callbacks_list = [checkpoint, tensorboard,lru_plato]
    vgg_model.model.fit(x=x_train,y=y_train,validation_split=0.2,epochs=24,batch_size=200,callbacks=[tensorboard])

    results = vgg_model.model.predict(x_test)
    y_pred = np.argmax(results,axis=1)
    print(f"accuracy_score:{accuracy_score(y_test, y_pred)}")
    conf_mat =  confusion_matrix(y_test,y_pred)
    labels_list = ['airplane','automobile','bird','cat','deer','dog','frog','horse','ship','truck']
    conf_mat_df =  pd.DataFrame(conf_mat, index = [i for i in labels_list],columns = [i for i in labels_list])
    plt.figure(figsize=(10, 7))
    sn.heatmap(conf_mat_df, annot=True)
    plt.show()

    dot_img_file = 'model_2.png'
    tf.keras.utils.plot_model(vgg_model.model, to_file=dot_img_file, show_shapes=True)


if __name__ == '__main__':
    main()


# 3. regularization - augmentation ; weights
#verbose..
# normalize RGB to values of vgg16
# maybe hsv?

